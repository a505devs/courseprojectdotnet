CREATE TABLE public."Products"
(
    "Id" serial NOT NULL,
    "Name" text NOT NULL,
    "Sku" text NOT NULL,
    "Quantity" integer NOT NULL,
    "Price" decimal(10,2) NOT NULL,
    CONSTRAINT "PK_Products" PRIMARY KEY ("Id")
);
