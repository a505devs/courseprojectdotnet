﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseProject.Core.Exceptions;

public class ProductNotFoundException : Exception
{
    public ProductNotFoundException(long id)
        : base($"Product with Id={id} not found") { }
}
