﻿using CourseProject.Core.Dtos;
using CourseProject.Core.Entities;

namespace CourseProject.Core.Abstractions;

public interface IProductService
{
    IReadOnlyCollection<ProductListItemDto> ShowProducts(string sortType = "",string searchString = "");


    void AddProduct(CreateProductDto createProductDto);
    void UpdateProduct(UpdateProductDto updateProductDto);
    void DeleteProduct(long id);
    ProductDto? GetProductById(long id);
    void ImportProduct(ImportProductDto importProductDto);
    IReadOnlyCollection<ExportProductDto> ExportProducts();
}
