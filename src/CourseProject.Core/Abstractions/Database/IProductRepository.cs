﻿using CourseProject.Core.Entities;

namespace CourseProject.Core.Abstractions.Database;

public interface IProductRepository
{
    IReadOnlyCollection<Product> GetProducts(string sortType, string searchString);

    Product? GetProductById(long id);

    void AddProduct(Product product);
    void UpdateProduct(Product product);
    void RemoveProductById(Product product);
}
