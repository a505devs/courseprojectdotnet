﻿using CourseProject.Core.Abstractions;
using CourseProject.Core.Abstractions.Database;
using CourseProject.Core.Dtos;
using CourseProject.Core.Entities;
using CourseProject.Core.Exceptions;

namespace CourseProject.Core.Services;

public class ProductService : IProductService
{
    private readonly IProductRepository _repository;

    public ProductService(IProductRepository repository)
    {
        _repository = repository;
    }

    public IReadOnlyCollection<ProductListItemDto> ShowProducts(string sortType = "",string searchString = "")
    {
        var answer = _repository.GetProducts(sortType, searchString)
            .Select(product =>
            {
                return new ProductListItemDto
                {
                    Id = (long) product.Id,
                    Name = product.Name,
                    Sku = product.Sku,
                    Price = product.Price,
                    Quantity = product.Quantity,
                    Total = product.Total,
                    CreatedDate = product.CreatedDate
                };
            })
            .ToList();
        return answer;
    }

    public void AddProduct(CreateProductDto createProductDto)
    {
        Product product = new(createProductDto.Id, createProductDto.Name, createProductDto.Sku, createProductDto.Quantity, createProductDto.Price);
        _repository.AddProduct(product);
    }

    public void UpdateProduct(UpdateProductDto updateProductDto)
    {
        var product = _repository.GetProductById(updateProductDto.Id);
        if (product == null) throw new ProductNotFoundException(updateProductDto.Id);
        product.Name = updateProductDto.Name;
        product.Sku = updateProductDto.Sku;
        product.Price = updateProductDto.Price;
        product.Quantity = updateProductDto.Quantity;
        _repository.UpdateProduct(product);
    }

    public void DeleteProduct(long id)
    {
        var product = _repository.GetProductById(id) ?? throw new ProductNotFoundException(id);
        _repository.RemoveProductById(product);
    }

    public ProductDto GetProductById(long id)
    {
        var product = _repository.GetProductById(id);
        var productDto = new ProductDto
        {
            Id = (long) product.Id,
            Name = product.Name,
            Sku = product.Sku,
            Quantity = product.Quantity,
            Price = product.Price,
            Total = product.Total
        };
        return productDto;
    }

    public void ImportProduct(ImportProductDto importProductDto)
    {
        Product product = new(importProductDto.Name, importProductDto.Sku, importProductDto.Quantity, importProductDto.Price);
        _repository.AddProduct(product);
    }

    public IReadOnlyCollection<ExportProductDto> ExportProducts()
    {
        return _repository.GetProducts("", "")
            .Select(product =>
            {
                return new ExportProductDto
                {
                    Id = (long) product.Id,
                    Name = product.Name,
                    Sku = product.Sku,
                    Quantity = product.Quantity,
                    Price = product.Price
                };
            })
            .ToList();
    }
}
