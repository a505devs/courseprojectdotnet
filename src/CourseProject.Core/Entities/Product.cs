﻿namespace CourseProject.Core.Entities;

public class Product
{
    private long? _id;
    private string _name;
    private string _sku;
    private int _quantity;
    private decimal _price;
    public DateTime CreatedDate { get; } = DateTime.Now;

    private Product() { }

    public Product(string name, string sku, int quantity, decimal price)
    {
        if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(sku) || quantity < 0 || price < 0)
            throw new ArgumentException("Invalid constructor parameters");

        _name = name;
        _sku = sku;
        _quantity = quantity;
        _price = price;
    }

    public Product(long id, string name, string sku, int quantity, decimal price)
        : this(name, sku, quantity, price)
    {
        Id = id;
    }

    public Product(Product product)
    {
        _name = product.Name;
        _sku = product.Sku;
        _quantity = product.Quantity;
        _price = product.Price;
    }

    public long? Id
    {

        set
        {
            if (value < 0) throw new ArgumentException(nameof(Id));
            _id = value;
        }
        get
        {
            return _id;
        }
    }

    public string Name
    {
        get => _name;
        set
        {
            if (string.IsNullOrEmpty(value))
                throw new ArgumentException(nameof(Name));
            _name = value;
        }
    }

    public string Sku
    {
        get => _sku;
        set
        {
            if (string.IsNullOrEmpty(value))
                throw new ArgumentException(nameof(Sku));
            _sku = value;
        }
    }

    public int Quantity
    {
        get => _quantity;
        set
        {
            if (value < 0)
                throw new ArgumentException(nameof(Quantity));
            _quantity = value;
        }
    }

    public decimal Price
    {
        get => _price;
        set
        {
            if (value < 0)
                throw new ArgumentException(nameof(Price));
            _price = value;
        }
    }

    public decimal Total => Price * Quantity;

    public override bool Equals(object? obj)
    {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj is not Product item)
            return false;

        return Id == item.Id
            && Quantity == item.Quantity
            && Price == item.Price
            && Name == item.Name
            && Sku == item.Sku;
    }

    public override string ToString()
    {
        return $"Product ID: {Id}\nProduct Name: {Name}\nProduct SKU: {Sku}\nProduct Sales Quantity: {Quantity}\nProduct Price: {Price}\nTotal sales amount: {Total}\n";
    }
}
