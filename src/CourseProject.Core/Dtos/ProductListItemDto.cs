﻿namespace CourseProject.Core.Dtos;

public class ProductListItemDto
{
    public long Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Sku { get; set; } = string.Empty;
    public int Quantity { get; set; }
    public decimal Price { get; set; }
    public decimal Total { get; set; }
    public DateTime CreatedDate { get; set; }
}
