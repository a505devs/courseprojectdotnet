﻿namespace CourseProject.Core.Dtos;
public class ImportProductDto
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string Sku { get; set; }
    public int Quantity { get; set; }
    public decimal Price { get; set; }
}
