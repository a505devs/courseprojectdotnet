﻿using CourseProject.Core.Abstractions;
using CourseProject.Core.Abstractions.Database;
using CourseProject.Core.Dtos;
using CourseProject.Core.Services;
using CourseProject.Infrastructure.Database;

IProductRepository productRepository = new InMemoryProductRepository();
IProductService productService = new ProductService(productRepository);

while (true)
{
    PrintMenu();

    int answer = Convert.ToInt32(Console.ReadLine());

    if (answer == 0) break;

    switch (answer)
    {
		case 1: ShowAllProducts(); break;
        case 2: AddProduct(); break;
        case 3: UpdateProduct(); break;
        case 4 :
            Console.Write("Введите ID товара, который хотите удалить: ");
            productService.DeleteProduct(Convert.ToInt32(Console.ReadLine()));
            break;
        default: Console.WriteLine("Приехали... Число от 1 до 4 ввести не можем. Ёмаё, куда катится этот мир..."); break;
    }
}

void PrintMenu()
{
    Console.WriteLine("1) Показать продукты");
    Console.WriteLine("2) Добавить продукт");
    Console.WriteLine("3) Редактировать продукт");
    Console.WriteLine("4) Удалить продукт");
    Console.WriteLine("0) Выход");
    Console.Write("Введите номер пункта меню: ");
}

void ShowAllProducts()
{
    foreach (var product in productService.ShowProducts())
    {
        Console.WriteLine($"{product.Id}: {product.Name} ({product.Sku}) {product.Quantity}x{product.Price}={product.Total}");
    }
}

void AddProduct()
{
    CreateProductDto dto = new ();
    Console.WriteLine("Введите параметры товара:");
    Console.Write("ID: ");
    dto.Id =Convert.ToInt32(Console.ReadLine());
    Console.Write("Название: ");
    dto.Name = Console.ReadLine();
    Console.Write("Артикул: ");
    dto.Sku = Console.ReadLine();
    Console.Write("Количество: ");
    dto.Quantity = Convert.ToInt32(Console.ReadLine());
    Console.Write("Цена: ");
    dto.Price = Convert.ToDecimal(Console.ReadLine());

    productService.AddProduct(dto);
}

void UpdateProduct()
{
    UpdateProductDto dto = new();

    Console.WriteLine("Введите данные об объекте для обновления.\nId: ");
    dto.Id = Convert.ToInt32(Console.ReadLine());
    Console.WriteLine("Название: ");
    dto.Name = Console.ReadLine();
    Console.WriteLine("Артикул: ");
    dto.Sku = Console.ReadLine();
    Console.WriteLine("Количество: ");
    dto.Quantity = Convert.ToInt32(Console.ReadLine());
    Console.WriteLine("Стоимость: ");
    dto.Price = (decimal)(Convert.ToDecimal(Console.ReadLine()));

    productService.UpdateProduct(dto);
}
