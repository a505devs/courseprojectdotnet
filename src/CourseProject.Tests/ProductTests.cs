using System;
using CourseProject.Core.Entities;
using Xunit;


namespace CourseProject.Tests
{
    public class ProductTests
    {
        [Fact]
        public void Product_Name1_PO54_18_500()
        {
            // arrange
            string Name = "Name1";
            string SKU = "PO54";
            int Quantity = 18;
            decimal Price = 500;
            decimal TotalExpected = 18 * 500;
            string ToStringExpected = $"Product ID: 10\nProduct Name: {Name}\nProduct SKU: {SKU}\nProduct Sales Quantity: {Quantity}\nProduct Price: {Price}\nTotal sales amount: {TotalExpected}\n";

            // act
            Product product = new(Name, SKU, Quantity, Price);
            product.Id = 10;
            string ToStringActual = product.ToString();
            decimal TotalActual = product.Total;

            // assert
            Assert.Equal(TotalExpected, TotalActual);
            Assert.Equal(ToStringExpected, ToStringActual);
        }

        [Fact]
        public void Product_byOtherProduct_with_Name2_AU64_13_1200()
        {
            // arrange
            string Name = "Name2";
            string SKU = "AU64";
            int Quantity = 13;
            decimal Price = 1200;

            // act
            Product product = new(Name, SKU, Quantity, Price);
            Product product2 = new(product);


            //assert
            Assert.Equal(product, product2);

        }

        [Fact]
        public void Product_Name3_FR891_negative4_100_throwsArgumentException()
        {
            // arrange
            string Name = "Name3";
            string SKU = "FR891";
            int Quantity = -4;
            decimal Price = 100;

            // assert
            Assert.Throws<ArgumentException>(() => new Product(Name, SKU, Quantity, Price));

        }
    }
}
