﻿using System.Collections.Generic;
using CourseProject.Core.Abstractions.Database;
using CourseProject.Core.Entities;
using CourseProject.Core.Services;
using Moq;
using Xunit;

namespace CourseProject.Tests;

public class ProductServiceTests
{
    [Fact]
    public void ShowProducts_ShouldReturnProductDtos_AndCorrectlyFillThem()
    {
        var testProducts = new List<Product>
        {
            new Product(1, "Чай", "SKU-001", 10, 56.4m),
            new Product(2, "Кофе", "SKU-002", 2, 25m)
        };

        // Используем https://github.com/moq/moq4
        var repositoryMock = new Mock<IProductRepository>();
        repositoryMock.Setup(repo => repo.GetProducts("", "")).Returns(testProducts);

        var service = new ProductService(repositoryMock.Object);

        var dtos = service.ShowProducts();

        Assert.Equal(2, dtos.Count);
        Assert.Collection(dtos,
            tea =>
            {
                Assert.Equal(1, tea.Id);
                Assert.Equal(564m, tea.Total);
            },
            coffee =>
            {
                Assert.Equal(2, coffee.Id);
                Assert.Equal(50m, coffee.Total);
            }
       );
    }
}
