﻿using CourseProject.Core.Abstractions;
using CourseProject.Core.Dtos;
using CourseProject.WebApp.Models;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Globalization;

namespace CourseProject.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductService _productService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(
            IProductService productService,
            ILogger<HomeController> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index(string sortType = "Id", string searchString = "")
        {
            var dtos = _productService.ShowProducts(sortType,searchString);


            return View(viewName: "Index", model: dtos);
        }

        [HttpPost]
        public async Task<IActionResult> ImportProducts(IFormFile uploadedFile)
        {
            using var reader = new StreamReader(uploadedFile.OpenReadStream());
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);

            var records = csv.GetRecordsAsync<ImportProductDto>();

            await foreach(var record in records)
            {
                _productService.ImportProduct(record);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> ExportProducts()
        {
            var records = _productService.ExportProducts();

            using var memoryStream = new MemoryStream();
            using var writer = new StreamWriter(memoryStream);
            using var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);

            await csv.WriteRecordsAsync(records);
            await writer.FlushAsync();

            return new FileStreamResult(new MemoryStream(memoryStream.ToArray()),"text/csv") { FileDownloadName = "ExportedTable.csv" };
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(viewName: "Create");
        }

        [HttpPost]
        public IActionResult Create(CreateProductDto dto)
        {
            _productService.AddProduct(dto);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var model = _productService.GetProductById(id);
            return View(viewName: "Edit", model: model);
        }

        [HttpPost]
        public IActionResult Edit(UpdateProductDto dto)
        {
            _productService.UpdateProduct(dto);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long id)
        {
            _productService.DeleteProduct(id);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
