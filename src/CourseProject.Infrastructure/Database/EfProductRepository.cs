﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseProject.Core.Abstractions.Database;
using CourseProject.Core.Entities;
using CourseProject.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace CourseProject.Infrastructure.Database;

public class EfProductRepository : IProductRepository
{
    private readonly string _connectionString;

    public EfProductRepository(IConfiguration configuration)
    {
        _connectionString = configuration.GetConnectionString("Default");
    }

    public void AddProduct(Product product)
    {
        using (var db = new ProductContext(_connectionString))
        {
            db.Products.Add(product);
            db.SaveChanges();
        }
    }
    public Product? GetProductById(long id)
    {
        using (var db = new ProductContext(_connectionString))
        {
            return db.Products.FirstOrDefault(x => x.Id == id);
        }
    }
    public IReadOnlyCollection<Product> GetProducts(string sortType, string searchString)
    {
        using (var db = new ProductContext(_connectionString))
        {
            if (!string.IsNullOrEmpty(searchString))
                switch (sortType)
                {
                    case "Id": return db.Products.Where(product => product.Name.Contains(searchString) || product.Sku.Contains(searchString)).OrderBy(x => x.Id).ToList(); break;
                    case "Name": return db.Products.Where(product => product.Name.Contains(searchString) || product.Sku.Contains(searchString)).OrderBy(x => x.Name).ToList(); break;
                    case "Sku": return db.Products.Where(product => product.Name.Contains(searchString) || product.Sku.Contains(searchString)).OrderBy(x => x.Sku).ToList(); break;
                    case "Quantity": return db.Products.Where(product => product.Name.Contains(searchString) || product.Sku.Contains(searchString)).OrderBy(x => x.Quantity).ToList(); break;
                    case "Price": return db.Products.Where(product => product.Name.Contains(searchString) || product.Sku.Contains(searchString)).OrderBy(x => x.Price).ToList(); break;
                    case "Total": return db.Products.Where(product => product.Name.Contains(searchString) || product.Sku.Contains(searchString)).OrderBy(x => x.Price * x.Quantity).ToList(); break;
                        default: return db.Products.Where(product => product.Name.Contains(searchString) || product.Sku.Contains(searchString)).OrderBy(x => x.Id).ToList(); break;
                }
            else
                switch (sortType)
                {
                    case "Id": return db.Products.OrderBy(x => x.Id).ToList();
                    case "Name": return db.Products.OrderBy(x => x.Name).ToList();
                    case "Sku": return db.Products.OrderBy(x => x.Sku).ToList();
                    case "Quantity": return db.Products.OrderBy(x => x.Quantity).ToList();
                    case "Price": return db.Products.OrderBy(x => x.Price).ToList();
                    case "Total": return db.Products.OrderBy(x => x.Price * x.Quantity).ToList();
                    default: return db.Products.OrderBy(x => x.Id).ToList();
                }
        }
    }
    public void RemoveProductById(Product product)
    {
        using (var db = new ProductContext(_connectionString))
        {
            db.Products.Remove(product);
            db.SaveChanges();
        }
    }
    public void UpdateProduct(Product product)
    {
        using (var db = new ProductContext(_connectionString))
        {
            var _product = db.Products.FirstOrDefault(product1 => product1.Id == product.Id);
            _product.Name = product.Name;
            _product.Sku = product.Sku;
            _product.Quantity = product.Quantity;
            _product.Price = product.Price;

            db.SaveChanges();
        }
    }
}
