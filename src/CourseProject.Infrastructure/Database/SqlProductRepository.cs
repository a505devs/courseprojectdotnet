﻿using CourseProject.Core.Abstractions.Database;
using CourseProject.Core.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace CourseProject.Infrastructure.Database;

public class SqlProductRepository : IProductRepository
{
    private readonly string _connectionString;

    public SqlProductRepository(IConfiguration configuration)
    {
        _connectionString = configuration.GetConnectionString("Default");
    }

    public IReadOnlyCollection<Product> GetProducts(string sortType, string searchString)
    {
        using var connection = new NpgsqlConnection(_connectionString);

        var products = connection.Query<Product>("SELECT * FROM public.\"Products\"");

        return products.ToList();
    }

    public void AddProduct(Product product)
    {
        using var connection = new NpgsqlConnection(_connectionString);

        string sql = @$"
INSERT INTO public.""Products""(""Name"", ""Sku"", ""Quantity"", ""Price"")
VALUES (@Name, @Sku, @Quantity, @Price)";

        connection.Execute(sql, param: new
        {
            Name = product.Name,
            Sku = product.Sku,
            Quantity = product.Quantity,
            Price = product.Price
        });
    }


    public Product? GetProductById(long id)
    {
        using var connection = new NpgsqlConnection(_connectionString);

        var product = connection.Query<Product>($"SELECT * FROM public.\"Products\" WHERE \"Products\".\"Id\" = {id}").FirstOrDefault();

        return product;
    }
    public void RemoveProductById(Product product)
    {
        using var connection = new NpgsqlConnection(_connectionString);

        connection.Execute($"DELETE FROM public.\"Products\" WHERE \"Products\".\"Id\" = {product.Id}");
    }
    public void UpdateProduct(Product product)
    {
        using var connection = new NpgsqlConnection(_connectionString);

        string sql = $@"
UPDATE public.""Products""
SET ""Name"" = @Name,
    ""Sku"" = @Sku,
    ""Quantity"" = @Quantity,
    ""Price"" = @Price
WHERE ""Id"" = @Id";

        connection.Execute(sql, param: new
        {
            Name = product.Name,
            Sku = product.Sku,
            Quantity = product.Quantity,
            Price = product.Price,
            Id = product.Id
        });
    }
}
