﻿using CourseProject.Core.Abstractions.Database;
using CourseProject.Core.Entities;

namespace CourseProject.Infrastructure.Database;

public class InMemoryProductRepository : IProductRepository
{
    private readonly List<Product> _products = new();

    public InMemoryProductRepository()
    {
        _products.Add(new Product(1, "Чай", "SKU-001", 10, 56.4m));
        _products.Add(new Product(2, "Кофе", "SKU-002", 15, 25m));
        _products.Add(new Product(3, "Круассан", "SKU-004", 5, 23m));
        _products.Add(new Product(4, "Яблоко", "SKU-005", 15, 26m));
        _products.Add(new Product(5, "Апельсин", "SKU-006", 28, 17m));
        _products.Add(new Product(6, "Мандарин", "SKU-007", 17, 97m));
        _products.Add(new Product(7, "Абрикос", "SKU-008", 15, 41m));
        _products.Add(new Product(8, "Каштан", "SKU-003", 31, 87m));
    }


    public IReadOnlyCollection<Product> GetProducts()
    {
        return _products;
    }
    public Product? GetProductById(long id)
    {
        return _products.FirstOrDefault(product => product.Id == id);
    }

    public void AddProduct(Product product)
    {
        product.Id = _products.Max(p => p.Id) + 1;
        _products.Add(product);
    }

    public void UpdateProduct(Product product)
    {
        int index = _products.IndexOf(product);
        _products[index].Name = product.Name;
        _products[index].Sku = product.Sku;
        _products[index].Quantity = product.Quantity;
        _products[index].Price = product.Price;
    }

    public void RemoveProductById(Product product)
    {
        _products.Remove(product);
    }

    public IReadOnlyCollection<Product> GetProducts(string sortType, string searchString) => throw new NotImplementedException();
}
